<?php

class CreateImage {
	public static function draw($w = 600, $h = 480, $color = null, $type = 'jpg', $font_size = null, $text = null) {
		// Прописываем заголовок PNG-изображения
		header('Content-type: image/' . $type);
		
		$text = (!$text) ? "{$w}x{$h}" : $text ;
		$font_size = (!$font_size) ? (
			($w <= 50) ? 7 :(
				($w <= 100) ? 10 : 
				(
					($w <= 200) ? 20 : 
					(
						($w <= 1500) ? 30 : 50
					)
				)
			) 
		) : $font_size ;

		$CENTER = $w/2;
		$top_text = ($h / 2) + ($font_size/2);
		// Создаем изображение
		$im = imagecreatetruecolor($w, $h);
		imageantialias($im, true);
		$color_text = imagecolorallocate($im, 0, 0, 0);
		// Шрифт текстаs
		$FONT = __DIR__.'/fonts/arial.ttf';

		// Создаем цвета
		if($color != null){
			switch ($color) {
				case 'white': $color = imagecolorallocate($im, 0xFF, 0xFF, 0xFF); break; // белый
				case 'red': $color = imagecolorallocate($im, 0x99, 0x33, 0x33); break; // красный
				case 'green': $color = imagecolorallocate($im, 0x33, 0x99, 0x33); break; // зеленый
				case 'blue': 
					$color_text = $color = imagecolorallocate($im, 0xFF, 0xFF, 0xFF); // белый
					$color = imagecolorallocate($im, 0, 0, 255); break; // синий
				case 'yellow': $color = imagecolorallocate($im, 0xFF, 0xDD, 0x00); break; // желтый
				case 'grey': $color = imagecolorallocate($im, 0xDD, 0xDD, 0xDD); break; // серый
				default:
					$c = self::hex2RGB('#'.strtoupper($color));
					$color = imagecolorallocate($im, $c['red'], $c['green'], $c['blue']);
				break;
			}
		} else {
			$color = imagecolorallocate($im, 0xDD, 0xDD, 0xDD); // серый
		}

		// Заливаем изображение белым цветом
		imagefill($im, 1, 1, $color);
		// imageline($im, 0, $h/2, $w, $h/2, $red);
		// imageline($im, $w/2, 0, $w/2, $h, $red);

		// размеры текст по координатам
		$box = imagettfbbox($font_size, 0, $FONT, $text);
		// размер отступа влево, чтобы текст оказался посередине заданной точки
		$left = $CENTER-round(($box[2]-$box[0])/2);
		imagettftext($im, $font_size, 0, $left, $top_text, $color_text, $FONT, $text);

		// Выводим изображение
		// Save the image to file and free memory
		if($type == "jpg") imagejpeg($im);
		if($type == "gif") imagegif($im);
		if($type == "png") imagepng($im);
		
		// очищаем память
		imagedestroy($im);
	}

	private static function hex2RGB($hexStr, $returnAsString = false, $seperator = ',') {
		$hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
		$rgbArray = array();

		if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
			$colorVal = hexdec($hexStr);
			$rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
			$rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
			$rgbArray['blue'] = 0xFF & $colorVal;
		} elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
			$rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
			$rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
			$rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
		} else {
			return false; //Invalid hex color code
		}

		return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
	}
}