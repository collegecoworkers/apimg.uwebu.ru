<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'genimg/image.php';

$app = new \Slim\App(new \Slim\Container([
	'settings' => [
		'displayErrorDetails' => true,
	],
]));

function check_attr($attr, $default = null){
	if(isset($attr) && $attr != null){
		return $attr;
	} else {
		return $default;
	}
}

function get_request(Request $request = null, Response $response = null) {

	$w = check_attr($request->getAttribute('w'), 300);
	$h = check_attr($request->getAttribute('h'), 200);
	$color = check_attr($request->getAttribute('color'), 'grey');
	$type = check_attr($request->getAttribute('type'), 'jpg');
	$response->write(CreateImage::draw($w, $h, $color, $type));

	return $response->withHeader('Content-Type', 'image/'.$type);
	return $response;
}

$callback = 'get_request';

$app->get('/', function ($req, $res, $args) {
  return $res->withStatus(302)->withHeader('Location', 'test.php');
});

$app->get('/{w}x{h}', $callback);
$app->get('/{w}x{h}/', $callback);
$app->get('/{w}x{h}/{color}', $callback);
$app->get('/{w}x{h}/{color}/', $callback);
$app->get('/{w}x{h}/{color}/{type}', $callback);

$app->run();
